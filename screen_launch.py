import asyncio
from my_socket import EsSocket
from datetime import datetime
import subprocess
import logging


scriptloc = "/opt/zm_event_listener/show_window.sh"
es = EsSocket("ws://znmndr.duckdns.org:9000")


def event_callback(message):
    # print(datetime.now(), message)
    # subprocess.Popen(['bash', str(scriptloc)],
    #     shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)
    events = message.get("events", [])
    logging.info(f"Calling bash: event-> {events}")
    res = subprocess.run(['bash', str(scriptloc)], capture_output=True, encoding="utf-8")
    logging.info(f"Bash returned: \n#### stdout\n{res.stdout}\n#### stderr\n{res.stderr}")


async def main():    
    while True:
        try:
            connected = await es.connect()
            if connected:
                await es.subscribe([4], [5], event_callback)
                await es.loop()
        except Exception as e:
            print(e)
        await es.close()
        print("sleeping 15 seconds before reconnecting")
        await asyncio.sleep(15.0)

if __name__ =="__main__":
    asyncio.run(main())