import asyncio
from datetime import datetime
import websockets
import environs
import json
import logging

class EsSocket:
    def __init__(self, url):
        self._sock = None
        self._url = url
        self._vers = ""
        env = environs.Env()
        env.read_env()
        self._user = env("ZM_USER")
        self._pass = env("ZM_PASS")
        self._cb = lambda x: None
        logging.basicConfig(filename=env("HOME")+"/log/EsSock.log", encoding="utf-8", level=logging.INFO)

    async def connect(self):
        sock = await websockets.connect(self._url, ping_interval=None)
        if sock.open:
            self._sock = sock
            auth = {
            "event":"auth",
            "data":{
                "user": self._user,
                "password": self._pass
            }}
            suc, rcv = await self.command(auth)
            if suc:
                self._vers = rcv["version"]
                logging.info(f"{datetime.now()}: Connected: {self._vers}")
                return True
        logging.warning("Couldnt connect to ES Server")
        return False

    async def close(self):
        await self._sock.close()

    async def subscribe(self, monitor_list, interval_list, callback):
        if not self._sock.open:
            logging.error("try subscribe but socket not open!")
            return
        ml = ",".join([ str(x) for x in monitor_list])
        il = ",".join([str(x) for x in interval_list])
        subs = { 
            "event":"control",
            "data":{
                "type":"filter",
                "monlist": ml, 
                "intlist": il
            }}
        await self.command(subs, False)
        self._cb = callback
        logging.info(f"{datetime.now()}: subscribed")

    async def loop(self):
        try:
            async for msg in self._sock:
                logging.info(f"{datetime.now()}: {msg}")
                msg = json.loads(msg)
                self._cb(msg)
        except Exception as e:
            logging.error("Error during msg loop.", exc_info=True)
    
    async def command(self, command_dic, await_answer=True):
        coms = json.dumps(command_dic)
        await self._sock.send(coms)
        rcv = None
        if await_answer:
            rcv = json.loads(await self._sock.recv())
            if rcv["status"] == "Success":
                return True, rcv
        return False, rcv


if __name__ == "__main__":
    from datetime import datetime

    def bla(msg):
        print(datetime.now(), msg)

    
    async def test():

        es = EsSocket("ws://znmndr.duckdns.org:9000")
        await es.connect()

        await es.subscribe([4], [5], bla)

        await es.loop()
        
        print(" OVER ! ")

    asyncio.run(test())

